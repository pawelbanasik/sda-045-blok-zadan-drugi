package punkt1;

public class Math {



	public static int max(int a, int b) {

		if (a > b) {

			return a;
		} else {
			return b;
		}

	}

	public static int min(int a, int b) {

		if (a < b) {

			return a;
		} else {
			return b;
		}

	}

	public static int abs(int a) {

		if (a >= 0) {
			return a;
		} else {
			return a * (-1);
		}

	}
	
    public static int pow (int a, int n) {
        int wynik = 1;
        for(int i =0 ; i <n ;i ++) {
            wynik *= a;
        }
        return wynik;
    }

}
