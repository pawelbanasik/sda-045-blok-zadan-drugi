package punkt5;

public abstract class FamilyMember {

	private String name;

	
	
	public String getName() {
		return name;
	}


	public FamilyMember(String name) {
		super();
		this.name = name;
	}
	
	
	public abstract void introduceYourself() ;
		
	
		

	
	
}
