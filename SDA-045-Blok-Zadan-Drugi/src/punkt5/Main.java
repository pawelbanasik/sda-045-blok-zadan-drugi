package punkt5;

public class Main {
	
	private Father father1 = new Father("Heniek");
	private Son son1 = new Son("Pawel");
	
	
	public void introduceFamily() {
		
		FamilyMember[] members = new FamilyMember[] {father1, son1};
		
		for (FamilyMember member : members) {
			
			member.introduceYourself();
		}
	}
	

	public static void main(String[] args) {
		
		Main family = new Main();
		family.introduceFamily();
		family.father1.introduceYourself();
		
		

	}

}
